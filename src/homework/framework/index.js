import { BookstoreAccount, BookstoreBookStore, MailboxLayerCheck } from './services/index';

const bookstoreApiProvider = () => ({
  account: () => new BookstoreAccount(),
  bookstore: () => new BookstoreBookStore(),
});

const mailboxLayerApiProvider = () => ({
  check: () => new MailboxLayerCheck(),
});

export { bookstoreApiProvider, mailboxLayerApiProvider };
