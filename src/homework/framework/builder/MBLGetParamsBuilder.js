import faker from 'faker';
import { user } from '../config';

const MBLGetParamsBuilder = function MBLGetParamsBuilder() {
  this.addToken = function addToken(token) {
    this.token = token;
    return this;
  };
  this.addGoodToken = function addGoodToken() {
    this.addToken(user.mailboxlayer.token);
    return this;
  };
  this.addBadToken = function addBadToken() {
    this.addToken('');
    return this;
  };

  this.addEmail = function addEmail(email) {
    this.email = email.toLowerCase();
    return this;
  };
  this.addRndEmail = function addRndEmail() {
    this.addEmail(faker.internet.email());
    return this;
  };
  this.addBadEmail = function addEmail() {
    this.addEmail(faker.internet.userName());
    return this;
  };

  this.generate = function generate() {
    const fields = Object.getOwnPropertyNames(this);
    const data = {};
    fields.forEach((fieldName) => {
      if (this[fieldName] && typeof this[fieldName] !== 'function') {
        data[fieldName] = this[fieldName];
      }
    });
    return data;
  };
};

export { MBLGetParamsBuilder };
