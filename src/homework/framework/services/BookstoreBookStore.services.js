import supertest from 'supertest';

import { urls } from '../config/index';

const BookstoreBookStore = function BookstoreBookStore() {
  this.getBooks = async function getBooks() {
    const r = await supertest(`${urls.bookstore}`).get('/BookStore/v1/Books').set('Accept', 'application/json');
    return r;
  };
  this.getBook = async function getBook(isbn) {
    const r = await supertest(`${urls.bookstore}`).get(`/BookStore/v1/Book?ISBN=${isbn}`).set('Accept', 'application/json');
    return r;
  };
};

export { BookstoreBookStore };
