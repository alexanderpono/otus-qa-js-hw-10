import supertest from 'supertest';

import { urls } from '../config/index';

const BookstoreAccount = function BookstoreAccount() {
  this.postUser = async function postUser(user) {
    const r = await supertest(`${urls.bookstore}`)
      .post('/Account/v1/User')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .send(user);

    return r;
  };

  this.deleteUser = async function deleteUser(uuid, token) {
    const r = await supertest(`${urls.bookstore}`)
      .delete(`/Account/v1/User/${uuid}`)
      .set('Accept', 'application/json')
      .set('Authorization', `Bearer ${token}`);

    return r;
  };

  this.getUser = async function getUser(uuid, token) {
    const r = await supertest(`${urls.bookstore}`)
      .get(`/Account/v1/User/${uuid}`)
      .set('Accept', 'application/json')
      .set('Authorization', `Bearer ${token}`);

    return r;
  };

  this.authorize = async function authorize(user) {
    const r = await supertest(`${urls.bookstore}`)
      .post('/Account/v1/GenerateToken')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .send(user);

    return r;
  };
};

export { BookstoreAccount };
