import supertest from 'supertest';

import { urls } from '../config';

const MailboxLayerCheck = function MailboxLayerCheck() {
  this.get = async function get(getParams) {
    const { token = '', email = '' } = getParams;
    const r = await supertest(`${urls.mailboxlayer}`)
      .get(`/api/check?access_key=${token}&email=${email}&smtp=1&format=1`)
      .set('Accept', 'application/json');

    return r;
  };

  this.getLocal = async function getLocal(getParams) {
    const { token = '', email = '' } = getParams;
    const r = await supertest(`${urls.dummymailboxlayer}`)
      .get(`/api/check?access_key=${token}&email=${email}&smtp=1&format=1`)
      .set('Accept', 'application/json');

    return r;
  };
};

export { MailboxLayerCheck };
