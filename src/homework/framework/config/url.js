const urls = {
  bookstore: 'https://bookstore.toolsqa.com',
  mailboxlayer: 'http://apilayer.net',
  dummymailboxlayer: 'http://localhost:8888',
};

export { urls };
