import { expect, test } from '@jest/globals';
import { mailboxLayerApiProvider as apiProvider } from '../src/homework/framework';
import { MBLGetParamsBuilder as GetBuilder } from '../src/homework/framework/builder/MBLGetParamsBuilder';

describe('MailboxLayer', () => {
  test('1+1=2', () => {
    expect(1 + 1).toBe(2);
  });
  it('successfully checks email', async () => {
    const params = new GetBuilder().addGoodToken().addRndEmail().generate();
    const r = await apiProvider().check().get(params);
    expect(r.status).toEqual(200);
    expect(typeof r.body).toEqual('object');
    expect(r.body.email).toBe(params.email);
  });

  it('rejects checking email without apiKey', async () => {
    const params = new GetBuilder().addBadToken().addRndEmail().generate();
    const r = await apiProvider().check().get(params);
    expect(r.status).toEqual(200);
    expect(typeof r.body).toEqual('object');
    expect(r.body.success).toEqual(false);
    expect(typeof r.body.error).toEqual('object');
    expect(r.body.error.type).toEqual('missing_access_key');
  });
});

describe('MailboxLayer-parameterized', () => {
  test.each`
      paramsBuilder                                                     | testName                            | bodyField          | expectedVal
      ${new GetBuilder().addGoodToken().addRndEmail()}                  | ${'format_valid => true'}           | ${'format_valid'}  | ${true}       
      ${new GetBuilder().addGoodToken().addBadEmail()}                  | ${'format_valid => false'}          | ${'format_valid'}  | ${false}       
      ${new GetBuilder().addGoodToken().addEmail('username@yandex.ru')} | ${'user => "username"'}             | ${'user'}          | ${'username'}       
      ${new GetBuilder().addGoodToken().addEmail('username@yandex.ru')} | ${'email => "username@yandex.ru"'}  | ${'email'}         | ${'username@yandex.ru'}       
      ${new GetBuilder().addGoodToken().addEmail('username@yandex.ru')} | ${'domain => "yandex.ru"'}          | ${'domain'}        | ${'yandex.ru'}       
    `('$email - $testName', async ({
    // eslint-disable-next-line no-unused-vars
    paramsBuilder, testName, bodyField, expectedVal,
  }) => {
    const params = paramsBuilder.generate();
    const r = await apiProvider().check().get(params);
    expect(r.body[bodyField]).toEqual(expectedVal);
  });
});
